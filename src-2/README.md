Two-Level Directory Organization
================================

This example project is an executable program that bundles some of its functionality into a static library.

The `make` mechanism is setup in a more templated fashion:

- The base [Makefile](Makefile) simply invokes make in the child directories
- The [Makefile.inc](Makefile.inc) contains the global settings for `make` variables, e.g. CC, LDFLAGS
- The [Makefile.rules](Makefile.rules) contains template rules for compiling C source, etc.

The latter two files are included by the Makefile in each child directory.  Thus, editing the value of CC in [Makefile.inc](Makefile.inc) affects the entire project.

