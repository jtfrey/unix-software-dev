One-Level Directory Organization
================================

This example project is a simple executable program, organized as a single source directory.

A sequence of Makefile samples illustrate a progressively more complex (and flexible) way to use `make` to build this project.  Since none have a default filename, this example also illustrates the `-f` flag to `make`:

```
$ make -f Makefile.6
gcc -c   printargv.c
gcc -c   my_program.c
gcc -o my_program printargv.o my_program.o
```

