Two-Level Directory Organization, CMake
===========================================

This example illustrates how multi-level projects are modelled in CMake.  Similar to the [src-2](../src-2/) project, the top-level `CMakeLists.txt` declares global properties for the project and indicates into which subdirectories the build should recurse.  Each of those subdirectories gets its own `CMakeLists.txt` file to model the item(s) produced in that subproject.

All procedures to generate the build environment, build the project, and install it remain the same.

Precompiler Conditionals
------------------------

This variant of the source code includes conditionally-compiled debug statements.  If the CMAKE_BUILD_TYPE is set to `Debug` the program will automatically insert the corresponding debugging output.

Static versus Shared Libraries
------------------------------

The BUILD_SHARED_LIBS option is used to toggle between producing `libprintargv.a` or `libprintargv.so` in that subproject.  It is set to ON by default in the top-level [CMakeLists.txt](CMakeLists.txt).

CMake defaults to adding runpaths to all library and executable targets to ensure all shared library dependencies are located at runtime.  This is useful when libraries and executables are present in the build tree, but problematic when they are installed (we prefer to use LD_LIBRARY_PATH to help the runtime linker find everything, it's more modular).  The top-level [CMakeLists.txt](CMakeLists.txt) overrides the default value (FALSE) of the CMAKE_SKIP_INSTALL_RPATH variable so that when targets are installed runpaths are NOT added to them.

