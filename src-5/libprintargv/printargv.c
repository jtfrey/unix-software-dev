
#include "printargv.h"

#include <stdio.h>

void
printargv(
  int		argc,
  const char*	argv[]
)
{
  int		argn = 0;

#ifndef NDEBUG
  fprintf(stderr, "DEBUG: enter %s\n", __func__);
#endif

  while ( ++argn < argc ) {
    printf("%-5d %s\n", argn, argv[argn]);
  }

#ifndef NDEBUG
  fprintf(stderr, "DEBUG: exit %s\n", __func__);
#endif
}

