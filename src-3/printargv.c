
#include "printargv.h"

#include <stdio.h>

void
printargv(
  int		argc,
  const char*	argv[]
)
{
  int		argn = 0;

  while ( ++argn < argc ) {
    printf("%-5d %s\n", argn, argv[argn]);
  }
}

