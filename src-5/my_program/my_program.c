
#include "libprintargv/printargv.h"
#include <stdio.h>

int
main(
  int		argc,
  const char*	argv[]
)
{
#ifndef NDEBUG
  fprintf(stderr, "DEBUG: argc = %d\n", argc);
#endif
  printargv(argc, argv);
  return 0;
}

