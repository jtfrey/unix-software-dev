Unix Software Development Basics - Examples
===========================================

This repository contains starting points for the worked examples.

- [src-1](src-1/) contains a one-level directory organization scheme, with various Makefiles that increase in complexity
- [src-2](src-2/) contains a two-level directory organization scheme (library + executable)
- [src-3](src-3/) contains a one-level directory organization scheme to be used with the GNU Autotools
- [src-4](src-4/) contains a one-level directory organization scheme to be used with CMake
- [src-5](src-5/) contains a two-level directory organization scheme to be used with CMake (including additional source features and configuration options)

In addition, example configuration files used during the overview of VALET are present in the valet-2.0 and valet-2.1 directories.  The former presents functionality from version 2.0 of the utility, while the latter illustrates changes present in version 2.1.


