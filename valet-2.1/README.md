VALET 2.1 Examples
==================

VALET 2.1 introduces several additional file formats for VALET configuration files.

yaml
----

The valet-2.0/example5 configuration file, translated to YAML format.

directory
---------

The valet-2.0/example5 configuration file, translated to a directory-of-fragments format.

