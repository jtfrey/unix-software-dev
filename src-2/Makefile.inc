#
# Makefile.inc
# Global variables for subprojects
#

MAKEFILE_INC	:=$(abspath $(lastword $(MAKEFILE_LIST)))
SRCDIR		:=$(dir $(MAKEFILE_INC))

CC		= gcc
CPPFLAGS	+= -DVERSION=1.0
CFLAGS		+= -g -O3

LDFLAGS		+= -L/usr/lib64
LIBS		+= -lm

