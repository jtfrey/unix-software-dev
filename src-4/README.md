One-Level Directory Organization, CMake
===========================================

The one-level organization is duplicated here, but instead of using a hand-crafted Makefile CMake is used to describe the project and produce a working build environment.

Generate the Build Environment
------------------------------

Like Autotools, CMake prefers if you create a unique directory to hold each build of the source code:

```
$ mkdir build-gcc
$ cd build-gcc
$ CC=$(which gcc) cmake -D CMAKE_INSTALL_PREFIX:PATH=/home/1001/unix-software-dev ..
```

The `ccmake` command can also be used to perform an interactive configuration of the build environment.

If successful, the project is built on a Unix/Linux platform using the `make` utility:

```
$ make
```

If the program builds successfully, it can be installed (to, in this case, `/home/1001/unix-software-dev/bin`) via:

```
$ make install
```

Distributing your Source Code
-----------------------------

Since CMake does not add extraneous files to the source directory, a package for distribution is just an archive file of the directory (e.g. a gzip'ed tar archive).

Cleanup
-------

The `make` expert in CMake generates a default `clean` rule in the project Makefiles, so object code, dependencies, and targets can be scrubbed using

```
$ make clean
```

Naturally, for a build done in a unique directory full cleanup amounts to removing the directory itself.

