# Produce a library (note we don't include the "lib" prefix that Unix/Linux
# typically wants or the type extension ".so" or ".a" -- this is a base name
# and CMake will figure out what prefix/suffix is necessary for the OS in
# question):
if (${BUILD_SHARED_LIBS})
    add_library(printargv SHARED printargv.c)
else ()
    add_library(printargv printargv.c)
endif ()

# Associate header files with the library target:
set_target_properties(printargv PROPERTIES PUBLIC_HEADER "printargv.h")

# Installation should copy the library to the "lib" directory and public
# header files to the "include" directory:
install (
    TARGETS printargv DESTINATION ${CMAKE_INSTALL_LIBDIR}
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
