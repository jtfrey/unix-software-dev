
#include "printargv.h"

int
main(
  int		argc,
  const char*	argv[]
)
{
  printargv(argc, argv);
  return 0;
}

